import {Column, Entity, Index, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm'
import {Item} from './item'

@Entity()
@Index(['name'], {unique: true})
export class Project {
  @PrimaryGeneratedColumn()
  private id: number

  @OneToMany(type => Link, link => link.project, {cascade:  ['insert', 'remove', 'update']})
  links: Link[]

  @Column({type: 'varchar'})
  name: string
}

@Entity()
@Index(['project', 'name'], {unique: true})
export class Link {
  @PrimaryGeneratedColumn()
  private id: number

  @Column({type: 'varchar'})
  name: string

  @ManyToOne(type => Project, project => project.links, {onDelete: 'CASCADE'})
  project: Project

  @ManyToOne(type => Item, {cascade: ['insert']})
  item: Item
}
