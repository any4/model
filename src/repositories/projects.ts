import {Service} from 'typedi'
import {EntityManager, FindOneOptions} from 'typeorm'
import {Project} from '../entities/project'
import * as error from 'http-errors'

export interface FindProjectOptions extends FindOneOptions<Project> {
  project: string
}

@Service({transient: true})
export class Projects {
  constructor(private manager: EntityManager) {}

  async find(options: FindProjectOptions): Promise<Project> {
    const {project: name} = options
    const project = await this.manager.findOne(Project, {...options, where: {name}} as FindOneOptions)
    if (!project)
      throw error(404, `Project ${name} not found`, {reason: 'ProjectNotFound', project: name})
    return project
  }

  async create(name: string): Promise<Project> {
    if (0 < await this.manager.count(Project, {where: {name}}))
      throw error(409, `Project ${name} exists`, {reason: 'ProjectAlreadyExists', project: name})
    return this.manager.save(this.manager.create(Project, {name}))
  }

  async remove(options: FindProjectOptions): Promise<void> {
    await this.manager.remove(await this.find(options))
  }
}
