import {Service} from 'typedi'
import {EntityManager, FindManyOptions, FindOneOptions} from 'typeorm'
import {Link} from '../entities/project'
import {Item} from '../entities/item'
import {Projects} from '../index'
import * as error from 'http-errors'

export interface FindItemsOptions extends FindManyOptions<Item> {
  project: string
}

export interface FindItemOptions extends FindOneOptions<Item> {
  project: string
  item: string
}

@Service({transient: true})
export class Items {
  constructor(private manager: EntityManager) {}

  async list(options: FindItemsOptions): Promise<{name: string, item: Item}[]> {
    const links = await this.manager.find<Link>(Link, {
      ...options, relations: ['item', 'project'], where: {project: {name: options.project}}
    })
    return links.map(({name, item}) => ({name, item}))
  }

  async create(project: string, name: string, item: Item): Promise<Item> {
    const parent = await new Projects(this.manager).find({project, relations: ['links']})
    if (parent.links.find(({name: existing}) => existing === name))
      throw error(409, `Item ${name} already associated with project ${project}`, {reason: 'ItemAlreadyExists', project, item: name})
    parent.links.push(this.manager.create(Link, {name, item}))
    const [, result] = await Promise.all([this.manager.save(parent), this.manager.save(item)])
    return result
  }

  async remove(options: FindItemOptions): Promise<void> {
    const link = await this.manager.findOne<Link>(Link, {
      ...options, relations: ['item', 'project', 'project.links'], where: {project: {name: options.project}, name: options.item}
    })
    if (!link)
      throw error(404, `Item ${options.item} is not associated with project ${options.project}`, {
        reason: 'ItemNotFound', item: options.item, project: options.project
      })
    link.project.links = link.project.links.filter(({name}) => name !== options.item)
    await Promise.all([this.manager.save(link.project), this.manager.remove(link), this.manager.remove(link.item)])
  }
}
