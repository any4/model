import * as chai from 'chai'
import * as sinon from 'sinon-chai'
import * as promise from 'chai-as-promised'

chai.should()
chai.use(sinon)
chai.use(promise)
