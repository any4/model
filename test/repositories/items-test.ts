import {using} from '../connection'
import {Project, Link, Item, Projects, Items} from '../../src'
import {TestItem} from '../mocks'

describe('Items', () => {
  describe('list', () => {
    describe('for an existing project', () => {
      let project

      beforeEach(using(async em => project = await em.save(em.create(Project, {name: 'foo'}))))

      describe('with items', () => {
        beforeEach(using(async em => {
          project.links = [em.create(Link, {name: 'bar', item: await em.save(em.create(TestItem))})]
          await em.save(project)
        }))

        it('should resolve the item list', using(async em => {
          const [bar, ...rest] = await new Items(em).list({project: 'foo'})
          rest.should.deep.equal([])
          bar.should.have.property('name').eql('bar')
          bar.should.have.property('item').which.is.an.instanceOf(TestItem)
        }))
      })

      describe('without items', () => {
        it('should resolve an empty list', using(async em => {
          await new Items(em).list({project: 'foo'}).should.eventually.deep.equal([])
        }))
      })
    })

    describe('for a non-existing project', () => {
      it('should resolve with empty array', using(async em => {
        await new Items(em).list({project: 'foo'}).should.eventually.eql([])
      }))
    })
  })

  describe('create', () => {
    describe('for an existing project', () => {
      let project

      beforeEach(using(async em => project = await em.save(em.create(Project, {name: 'foo'}))))

      describe('a new item', () => {
        it('should resolve', using(async em => {
          await new Items(em).create('foo', 'bar', new TestItem())
        }))
      })

      describe('an existing item', () => {
        beforeEach(using(async em => {
          project.links = [em.create(Link, {name: 'bar', item: await em.save(em.create(TestItem))})]
          await em.save(project)
        }))

        it('should reject', using(async em => {
          await new Items(em).create('foo', 'bar', new TestItem()).should.be.rejectedWith('Item bar already associated with project foo')
        }))
      })

      afterEach(using(async em => {
        const [bar, ...rest] = await em.find(Link, {relations: ['item']})
        rest.should.deep.equal([])
        bar.should.have.property('name').eql('bar')
      }))
    })

    describe('for a non-existing project', () => {
      it('should reject', using(async em => {
        await new Items(em).create('foo', 'bar', new TestItem()).should.be.rejectedWith('Project foo not found')
      }))
    })
  })

  describe('remove', () => {
    describe('from an existing project', () => {
      let project

      beforeEach(using(async em => project = await em.save(em.create(Project, {name: 'foo'}))))

      describe('an existing item', () => {
        beforeEach(using(async em => {
          project.links = [em.create(Link, {name: 'bar', item: await em.save(em.create(TestItem))})]
          await em.save(project)
        }))

        it('should resolve', using(em => new Items(em).remove({project: 'foo', item: 'bar'})))
      })

      describe('a non-existing item', () => {
        it('should reject', using(async em => {
          await new Items(em).remove({project: 'foo', item: 'bar'}).should.be.rejectedWith('Item bar is not associated with project foo')
        }))
      })

      afterEach(using(async em => {
        await em.count(Item).should.eventually.equal(0)
        await em.count(Link).should.eventually.equal(0)
        const [foo, ...rest] = await em.find(Project, {relations: ['links']})
        rest.should.deep.equal([])
        foo.should.have.property('name').equal('foo')
        foo.should.have.property('links').deep.equal([])
      }))
    })

    describe('from a non-existing project', () => {
      it('should reject', using(async em => {
        await new Items(em).remove({project: 'foo', item: 'bar'}).should.be.rejectedWith('Item bar is not associated with project foo')
      }))
    })
  })
})
