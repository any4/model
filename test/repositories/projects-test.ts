import {using} from '../connection'
import {Project, Projects} from '../../src'

describe('Projects', () => {
  describe('create', () => {
    describe('a new project', () => {
      it('should resolve', using(async em => {
        await new Projects(em).create('foo')
      }))
    })

    describe('an existing project', () => {
      beforeEach(using(em => em.save(em.create(Project, {name: 'foo'}))))

      it('should reject', using(async em => {
        await new Projects(em).create('foo').should.be.rejectedWith('Project foo exists')
      }))
    })

    afterEach(using(em => em.find(Project).then(projects => {
      projects.should.be.instanceOf(Array).with.length(1)
      const [project] = projects
      project.should.have.property('name').equal('foo')
    })))
  })

  describe('find', () => {
    describe('an existing project', () => {
      let project

      beforeEach(using(async em => project = await em.save(em.create(Project, {name: 'foo'}))))

      it('should resolve', using(async em => {
        await new Projects(em).find({project: 'foo'}).should.eventually.deep.equal(project)
      }))
    })

    describe('a non-existing project', () => {
      it('should reject', using(async em => {
        await new Projects(em).find({project: 'foo'}).should.be.rejectedWith('Project foo not found')
      }))
    })
  })

  describe('remove', () => {
    describe('an existing project', () => {
      beforeEach(using(em => em.save(em.create(Project, {name: 'foo'}))))

      it('should resolve', using(async em => {
        await new Projects(em).remove({project: 'foo'})
      }))
    })

    describe('a non-existing project', () => {
      it('should reject', using(async em => {
        await new Projects(em).remove({project: 'foo'}).should.be.rejectedWith('Project foo not found')
      }))
    })

    afterEach(using(async em => {
      await em.count(Project).should.eventually.equal(0)
    }))
  })
})
