import {Item} from '../../src'
import {ChildEntity} from 'typeorm'

@ChildEntity()
export class TestItem extends Item {}
