import {createConnection, getConnectionOptions, Connection, EntityManager} from 'typeorm'
import {TestItem} from './mocks/test-item'
import {Project, Link, Item} from '../src'
import {ConnectionOptions} from 'typeorm/connection/ConnectionOptions'
import 'reflect-metadata'
import 'sql.js'

let connection: Connection

before(async () => {
  const config: ConnectionOptions = await getConnectionOptions().catch(() => ({
    type: 'sqljs', location: 'testdb', autoSave: true, synchronize: true
  }))
  const options = {...config, entities: [Project, Link, Item, TestItem]}
  connection = await createConnection(options)
})

beforeEach(async () => {
  await connection.dropDatabase()
  await connection.synchronize()
})

export function using(fn: (transaction: EntityManager) => Promise<any>): () => Promise<any> {
  return () => connection.transaction(fn)
}
