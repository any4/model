const {NormalModuleReplacementPlugin, ProvidePlugin} = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const {fileSync} = require('find')
const {join} = require('path')

module.exports = {
  mode: 'development',
  entry: fileSync(/-test\.ts/, __dirname + '/test'),
  output: {
    path: __dirname + '/test',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  devServer: {
    static: './test',
    port: 8888
  },
  stats: 'errors-only',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: [/node_modules/],
        loader: 'ts-loader'
      }
    ]
  },
  plugins: [
    new NormalModuleReplacementPlugin(/typeorm$/, result => {
      result.request = result.request.replace(/typeorm/, 'typeorm/browser')
    }),
    new ProvidePlugin({
      'window.SQL': join(__dirname, 'node_modules/sql.js/dist/sql-asm.js')
    }),
    new CopyWebpackPlugin({
      patterns: [
        {from: 'node_modules/mocha/mocha.css', to: 'mocha.css'},
        {from: 'node_modules/mocha/mocha.js', to: 'mocha.js'},
        {from: 'node_modules/jquery/dist/jquery.min.js', to: 'jquery.js'}
      ]
    })
  ],
  resolve: {
    fallback: {
      fs: false,
      path: false,
      crypto: false
    }
  },
  externals: {
    'react-native-sqlite-storage': 'react-native-sqlite-storage'
  }
}
